# !/usr/bin/python
# Filename: objevar.py

class Robot:
    '''描述一个机器人，带名字的'''

    population =0

    def __init__(self, name):
        '''初始化数据'''
        self.name = name
        print('(初始化 {0})'.format(self.name))

        Robot.population +=1

    def __del__(self):
        '''我将消失'''
        print('{0}开始消毁'.format(self.name))
        Robot.population -=1

        if Robot.population == 0:
            print('{0} 是最后一个机器人'.format(self.name))
        else:
            print('这里还有{0:d}个机器人！'.format(Robot.population))

    def sayHi(self):
        '''机器人发声。

        㖿！它可以说话了。'''

        print('您好，我的名字叫{0}'.format(self.name))
        
    @staticmethod
    def howMany():
        '''打印当前机器人数。'''

        print('我们现在有{0:d}个机器人'.format(Robot.population))

    # howMany = staticmethod(howMany)  #静态函数实现的另一个方式


droid1 = Robot('R2-D2')
droid1.sayHi()

Robot.howMany()

droid2 = Robot('C-3P0')
droid2.sayHi()

Robot.howMany()

print("\n机器人正在做一些事情。\n")

print("机器人完成了他们的使命，将消毁他们……")

del(droid1)

del(droid2)

Robot.howMany()

