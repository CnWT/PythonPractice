# !/usr/bin/pyton
# Filename: inherit.py


class SchoolMember:
    '''描绘任何一个学校成员'''

    def __init__(self, name, age):
        self.name = name
        self.age = age
        print('(初始化学校成员：{0})'.format(self.name))

    def tell(self):
        '''告诉我详情'''
        print('姓名："{0}" 年龄："{1}" '.format(self.name, self.age), end='')


class Teacher(SchoolMember):
    '''描绘老师'''

    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)
        self.salary = salary
        print('(初始化老师：{0})'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('薪水:"{0:d}" '.format(self.salary))


class Student(SchoolMember):
    '''描绘学生'''

    def __init__(self, name, age, marks):
        SchoolMember.__init__(self, name, age)
        self.marks = marks
        print('(初始化学生：{0}'.format(self.name))

    def tell(self):
        SchoolMember.tell(self)
        print('分数:"{0:d}" '.format(self.marks))


t = Teacher("汤老师", 30, 30000)
s = Student("陈明", 25, 75)

print()  #打印一空行

members = [t, s]
for member in members:
    member.tell()
