# !/usr/bin/python
# Filename: class_init.py

class Person:
    def __init__(self, name):
        self.name = name

    def sayHi(self):
        print('你好，我的名字是', self.name)

p = Person('汤坤')
p.sayHi()
