# !/usr/bin/python
# Filename: using_tuple.py

zoo = ('蟒蛇', '大象', '企鹅')

print('动物园的动物数量是', len(zoo))

new_zoo = ('猴子', '鳄鱼', zoo)

print('新动物园的新增的动物数量',  len(new_zoo)-1)
print('新动物园的所有动物分别是：', new_zoo)
print('来自老动物园的动物分别是：',  new_zoo[2])
print('老动物园的最后一只动物是：', new_zoo[2][2])
print('新动物园的数量为：', len(new_zoo)-1+len(new_zoo[2]))