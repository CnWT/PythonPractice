# !/ur/bin/python
# Filename: using_list.py

# 这是一个购物列表

shoplist = ['苹果', '芒果', '西红柿', '香蕉']

print('我有', len(shoplist),'样东西要买。')

print('这些物品是：', end=' ')
for item in shoplist:
    print(item, end=' ')

print('\n我还要买蛋糕')
shoplist.append('蛋糕')
print('我的购物清单又更新了', shoplist)

print('我现在对购物清单进行排序')
shoplist.sort()
print('排序后的清单是', shoplist)
print('我将购买第一个物品是', shoplist[0])
olditem = shoplist[0]
del shoplist[0]
print('我购买的物品是', olditem)
print('我购物物品后的清单', shoplist)
