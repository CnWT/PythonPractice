# !/usr/bin/python
# Filename: reference.py

print('简单的分配')
shoplist = ['苹果', '芒果', '西瓜', '香蕉']
mylist = shoplist #mylist只是指定同一个对象的另一个名称

del shoplist[0]

print('shoplist is', shoplist)
print('mylist is ', mylist)

print('复制全切片')

mylist = shoplist[:]
del mylist[0]

print('shoplist is', shoplist)
print('mylist is', mylist)


