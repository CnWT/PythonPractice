# !/usr/bin/python
# Filename: using_dict.py

ab = {
    '汤坤':'cicw@qq.com',
    '张大云':'zdy@qq.com',
    '叶星':'yx@qq.com',
    '卢峥':'lz@qq.com',
    '余润波':'yyb@qq.com'
}

print('汤坤的邮箱是：', ab['汤坤'])

del ab['余润波']

print('\n 在这个联系簿中有{0}个联系人'.format(len(ab)))

for name, address in ab.items():
    print('联系人 {0} 的邮箱 {1}'.format(name, address))

ab['余润波'] = 'yyb@qq.com'

if '余润波' in ab:
    print('\n 余润波的邮箱是', ab['余润波'])
