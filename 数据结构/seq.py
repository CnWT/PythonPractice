# !/usr/bin/pyton
# Filename: seq.py

shoplist = ['苹果', '芒果', '西瓜', '香蕉']
name = 'tangkun'

print('项目0是', shoplist[0])
print('项目1是', shoplist[1])
print('项目2是', shoplist[2])
print('项目3是', shoplist[3])
print('项目-1是', shoplist[-1])
print('项目-2是', shoplist[-2])
print('项目字符0是', name[0])


print('项目1到3是', shoplist[1:3])
print('项目2到结尾是', shoplist[2:])
print('项目1到-1是', shoplist[1:-1])
print('项目开始到结尾是', shoplist[ : ])

print('字符1到3是', name[1:3])
print('字符2到结尾是', name[2:])
print('字符1到-1是', name[1:-1])
print('字符开始到结尾是', name[ : ])


