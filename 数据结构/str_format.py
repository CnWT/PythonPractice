#!/usr/bin/python
# Filename: str_format.py

age = 25
name = "汤坤"

print('{0}是{1} 岁'.format(name, age)) #变量名
print('{0}是玩Python的吗？'.format(name)) #变量名
print('1除以3等于{0:.3}'.format(1/3)) #3个小数位
print('{0:_^11}'.format('汤坤')) #使用下划线填充文本
