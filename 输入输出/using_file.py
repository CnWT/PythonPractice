# !/usr/bin/python3
# Filename: using_file.py

poem = '''\
Programing is fun
when the work is done
if you wanna make your work also fun:
    user Python!
'''

f = open('poem.txt', 'w')  #用写方式打开文件
f.write(poem)
f.close()

f = open('poem.txt') #如果没有指定打开模式，就是默认的读方式
while True:
    line = f.readline()
    if len(line) == 0:
        break
    print(line, end='\n')
f.close()


