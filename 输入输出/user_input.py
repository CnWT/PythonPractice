# !/usr/bin/python3
# Filename: user_input.py

import string

def reverse(text):
    return text[::-1]

def is_palindrom(text):
    text = text.lower()
    text = text.replace(' ','')
    for char in string.punctuation:
        text = text.replace(char, '')

    return text == reverse(text)

def main():
    # something = input('输入文本:')
    something = "aba"
    if (is_palindrom(something)):
        print("“{0}”是回文".format(something))
    else:
        print("“{0}”不是回文".format(something))

if __name__ == '__main__' :
    main()
else:
    print("user_input.py是被导入为模块")

