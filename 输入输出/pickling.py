# !/usr/bin/python3
# Filename: pickling.py

import pickle

shoplistfile = 'shoplist.data'

shoplist = ['苹果', '芒果', '香蕉']

f = open(shoplistfile, 'wb')
pickle.dump(shoplist, f)
f.close()

del shoplist

f = open(shoplistfile, 'rb')
storedlist = pickle.load(f)
print(storedlist)