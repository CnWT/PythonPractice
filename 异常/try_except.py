# !/usr/bin/python3
# Filename: try_except.py

class ShortInputException(Exception):
    '''用户自定义异常类'''

    def __init__(self, length, atleast):
        Exception.__init__(self)
        self.length = length
        self.atleast = atleast

try:
    text = input('输入一些文本--->')
    if len(text) < 3:
        raise ShortInputException(len(text), 3)
except EOFError:
    print('错误的结尾')
except KeyboardInterrupt:
    print('你做了放弃操作。')
except ShortInputException as ex:
    print('ShortInputException 输入长度为{0}， 异常！至少为{1}'.format(ex.length, ex.atleast))
else:
    print('没有异常抛出\n你输入了:{0}'.format(text))