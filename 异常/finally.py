# !/usr/bin/python3
# Filename: finally.py

import time

try:
    f = open('poem.txt')
    while True:
        line = f.readline()
        if len(line) == 0:
            break
        print(line, end = '')
        time.sleep(2)

except KeyboardInterrupt:
    print('!!读取文件时你做放弃操作')
finally:
    f.close()
    print('（清理）关闭这个文件')