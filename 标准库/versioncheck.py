# !/usr/bin/python3
# Filename: versioncheck.py

import sys, warnings

if sys.version_info[0] < 3:
    warnings.warn('该程序需要在Python 3.0以上才能运行', RuntimeWarning)
else:
    print('运行正常')