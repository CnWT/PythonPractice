# ！/usr/bin/python3
# Filename: use_logging.py

import os, platform, logging, sys
import logzero
from logzero import logger

if platform.platform().startswith('Windows'):
    logging_file = os.path.join(sys.path[0],'test.log')
else:
    logging_file = os.path.join(os.getenv('HOME'), 'test.log')

logging.basicConfig(
    level = logging.DEBUG,
    format = '%(asctime)s:%(levelname)s:%(message)s',
    filename = logging_file,
    filemode = 'a',
)
logzero.loglevel(logging.DEBUG) #设置最小的日志级别
# logzero.logfile(logging_file) #设置保存的日志文件
# logzero.logfile(logging_file, loglevel=logging.ERROR) #设置保存的日志文件,设置不同的级别
logzero.logfile(logging_file, mode='a',  maxBytes=1e6, backupCount=3) #设置保存的日志文件,设置日志文件最大字节数以及备份的次数
# logzero.logfile(None) #禁止日志输出到文件

logging.debug("程序启动")
logging.info("做一些事情")
logging.warning("程序有点问题")

logger.debug("您好") #这个日志信息是输入给控制台
logger.info("您好") #这个日志信息是输入给控制台
logger.error ("您好") #这个日志信息是输入给控制台
logger.warn ("您好") #这个日志信息是输入给控制台

#设置自定义格式
formatter = logging.Formatter(' %(name)s - %(asctime)-15s-%(levelname)s:%(message)s ')
logzero.formatter(formatter)

logger.info("var1:%s, var2:%s", "程序启动", "测试")


