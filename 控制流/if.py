#!/usr/bin/python
# Filename: if.py

number = 23
guess = int(input('输入一个整数：'))

if guess == number:
    print("恭喜您！你猜中了。")
elif guess < number:
    print('猜错了，比那个数要低些')
else:
    print('猜错了，比那个数要高些')

print('猜数结束！') 