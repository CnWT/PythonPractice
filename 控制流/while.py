# !/usr/bin/python
# Filename while.py

number = 23
running = True

while running:
    guess = int(input("输入一个整数:"))

    if guess == number:
        print("恭喜你，你猜对了。")
        running = False  #猜对了将导致循环停止
    elif guess < number:
        print("您猜的数比它低！")
    else:
        print("您猜的数比它高！")

else:
    print("这个循环将结束。")  #不符合任何条件将写在这里

print("结束")