#!/usr/bin/python
# Filename: func_key.py

def func(a, b=5, c=10):
    print('a为', a, 'b为', b, 'c为', c)

func(3)
func(3, 7)
func(25, c=24)
func(c=25, a=100)