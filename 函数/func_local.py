#!/usr/bin/python
# Filename: func_local.py

x = 50

def func(x):
    print('x值为：', x)
    x = 2
    print('改变局部变量x为：', x)

func(x)

print('x值仍然为：', x)