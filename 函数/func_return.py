#!/usr/bin/python
# Filename: func_return.py

def maximum(x, y):
    if x > y:
        return x
    else:
        return y

def someFunction():
    #每个函数都在结尾暗含有一个return None语句。
    pass

print(maximum(2 ,3))
print(someFunction())