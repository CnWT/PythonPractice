#!/usr/bin/python
# Filename: func_doc.py

def printMax(x, y):
    '''打印两个数的最大数。  

    两个数必须是整型'''

#个函数的文档说明，文档字符串的惯例是一个多行字符串，它的首行以大写字母开始，句号结尾。第
#二行是空行，从第三行开始是详细的描述。强烈建议你在你的函数中使用文档字符串
#时遵循这个惯例。

    x = int(x)  #如果可能，转换为整型
    y = int(y)

    if x > y:
        print(x, '为最大数') 
        return
    else:
        print(y, '为最大数') 
        return
    print('两数相等')

printMax(3, 5)
print(printMax.__doc__)