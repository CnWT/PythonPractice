#!/usr/bin/python
# Filename: func_global.py

x = 50

def func():
    global x

    print('x值为：', x)
    x = 2
    print('改变全局变量x值为', x)

func()
print('x值为：', x)