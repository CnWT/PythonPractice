#!/usr/bin/python
# Filename: func_varargs.py

def total(initial = 5, *numbers, **keywords):
    #当我们定义一个带星的参数，像 *param 时，从那一点后所有的参数被收集为一个叫做 ’param’ 的列表
    #当我们定义一个带两个星的参数，像 **param 时，从那一点开始的所有的关键字参数会被收集为一个叫做 ’param’ 的字典
    count = initial
    for number in numbers:
        count += number
    for key in keywords:
        count += keywords[key]
    return count

print(total(10, 1, 2, 3, vegettables=50, fruits = 100))
