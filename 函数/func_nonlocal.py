#!/usr/bin/python
# Filename: func_nonlocal.py

def func_outer():
    x = 2
    print('x值为', x )

    def func_inner():
        nonlocal x
        x = 5

    func_inner()
    print('改变局部变量x为', x)

func_outer()