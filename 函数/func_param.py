#!/usr/bin/python
# Filename: func_param.py

def printMax(a, b):
    if a > b:
        print(a, '是最大值')
    elif a == b:
        print(a, '等于', b)
    else:
        print(b,'是最大值')

printMax(3, 4)

x = 5 
y = 7

printMax(x, y)
